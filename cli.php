<?php
require __DIR__ . '/vendor/autoload.php';
if (PHP_SAPI == 'cli') {
    $argv = $GLOBALS['argv'];
    array_shift($argv);
    $pathInfo       = implode('/', $argv);

    $env = \Slim\Http\Environment::mock(['REQUEST_URI' => $pathInfo]);

    $settings = require __DIR__ . '/src/settings.php';
    $settings['environment'] = $env;
    $app = App\ConsoleApp::getInstance()->productSync();

}