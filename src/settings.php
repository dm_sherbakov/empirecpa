<?php
define('APP_ROOT', __DIR__);
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        'static_path' => APP_ROOT . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR,
        'web_static_path' => '/static/',
        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        'doctrine' => [
            'meta' => [
                'entity_path' => [
                    'src/app/entity'
                ],
                'auto_generate_proxies' => true,
                'proxy_dir' =>  __DIR__.'/../cache/proxies',
                'cache' => null,
            ],
            'connection' => [
                'driver' => 'pdo_mysql',
                'host' => 'localhost',
                'port' => 3306,
                'dbname' => 'empireCPA',
                'user' => 'root',
                'password' => '',
                //'charset' => 'utf-8'
            ]
        ],
        'refs' => [
            'geo' => [
                'request' => [
                    1 => 'RUS',
                    2 => 'KZ'
                ]
            ]
        ],
        'integrations' => [
            App\Integrations\EmpireCpa::class => [
                'servers' => [
                    'RUS' => 'http://mng.sweet-dom.com/',
                    'KZ' => 'http://mng.postel-kz.com/'
                ]
            ]
        ]
    ],
];
