<?php
namespace App;

class ProductsFilterDTO
{
    public $catId;
    public $kitNumber;
    public $geoId;

    public static function load(array $data){
        $filter = new self;
        if(array_key_exists('catId', $data)) $filter->catId = $data['catId'];
        if(array_key_exists('kitNumber', $data)) $filter->kitNumber = $data['kitNumber'];
        if(array_key_exists('geoId', $data)) $filter->geoId = $data['geoId'];
        return $filter;
    }
}