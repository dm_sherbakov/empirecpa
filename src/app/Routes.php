<?php
namespace App;
use App\Entity\Products;
use App\Entity\ProductsKits;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
class Routes
{
    public $app;
    public function __construct(\Slim\App $app)
    {
        $this->app = $app;
    }

    public function init(){
        $this->app->get('/[{view}]', function (Request $request, Response $response, array $args) {
            $args['response'] = $response;
            if(!array_key_exists('view', $args)){
                $view = 'index';
            } else {
                $view = $args['view'];
            }
            switch ($view){
                case "index":
                    $filter = new ProductsFilterDTO();
                    $filter->kitNumber = 1;
                    $products = \App\Products::getByFilter($filter);
                    $args['products'] = $products;
                    break;
                case "product_list":
                    $attributes = $request->getAttributes();
                    $filter = ProductsFilterDTO::load($attributes);
                    $products = \App\Products::getByFilter($filter);
                    $args['products'] = $products;
            }
            try {
                return $this->renderer->render($response, $view . '.php', $args);
            } catch(\RuntimeException $ex){
                return $this->renderer->render($response, '404.html', $args);
            }
        });
    }

}