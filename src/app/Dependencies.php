<?php

namespace App;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\Setup;
use GuzzleHttp\Client;
use Slim\Container;

class Dependencies
{
    public static function init(\Slim\App $app = null)
    {
        if($app == null) {
            $app = BaseApp::getInstance()->getSlim();
        }
        $container = $app->getContainer();

        $container['renderer'] = function ($c) {
            $settings = $c->get('settings')['renderer'];
            return new \App\PhpRenderer($settings['template_path']);
        };

        $container['logger'] = function ($c) {
            $settings = $c->get('settings')['logger'];
            $logger = new \Monolog\Logger($settings['name']);
            $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
            $logger->pushHandler(new \Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
            return $logger;
        };

        $container['em'] = $container[EntityManagerInterface::class] = function (Container $container) {

            $settings = $container->get('settings');
            $config = Setup::createAnnotationMetadataConfiguration(
                $settings['doctrine']['meta']['entity_path'],
                $settings['doctrine']['meta']['auto_generate_proxies'],
                $settings['doctrine']['meta']['proxy_dir'],
                $settings['doctrine']['meta']['cache'],
                false
            );
            return EntityManager::create($settings['doctrine']['connection'], $config);
        };

        $container['syncService'] = function(Container $container){
            return new ProductSyncService($container);
        };

        $container['httpAdapter'] = new Client();
    }
}