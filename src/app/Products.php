<?php

namespace App;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity;
use Doctrine\ORM\Query\Expr\Join;

class Products
{
    public static function getByFilter(ProductsFilterDTO $filter)
    {
        $products = array();
        $em = App::getInstance()->getSlim()->getContainer()->get(EntityManagerInterface::class);
        if ($em instanceof EntityManagerInterface) {
            $builder = $em->createQueryBuilder();
            $builder->select(
                'p.id as product_id',
                'p.name',
                'p.img',
                'p.composition',
                'm.name as material',
                'e.name as event',
                'geo.country',
                'pk.kit_id',
                'pk.kit_number',
                'pk.price as kit_price',
                'pk.old_price as old_kit_price',
                'ki.count as item_count',
                'i.name as item_name',
                'i.height as item_height',
                'i.width as item_width'
            )
                ->from(\App\Entity\Products::class, 'p')
                ->leftJoin(
                    Entity\Events::class,
                    'e',
                    Join::WITH,
                    'p.event_id = e.id'
                )
                ->leftJoin(
                    Entity\Materials::class,
                    'm',
                    Join::WITH,
                    'p.material_id = m.id'
                )
                ->leftJoin(
                    Entity\Geo::class,
                    'geo',
                    Join::WITH,
                    'geo.id = p.geo_id'
                )
                ->leftJoin(
                    Entity\ProductsKits::class,
                    'pk',
                    Join::WITH,
                    'p.id = pk.product_id'
                )
                ->leftJoin(
                    Entity\KitsItems::class,
                    'ki',
                    Join::WITH,
                    'pk.kit_id = ki.kit_id'
                )
                ->leftJoin(
                    Entity\Items::class,
                    'i',
                    Join::WITH,
                    'i.id = ki.item_id'
                );
            if ($filter->kitNumber) {
                $builder->andWhere('pk.kit_number = :kit_number');
                $builder->setParameter('kit_number', $filter->kitNumber);
            }
            if ($filter->catId) {
                $builder->andWhere('p.cat_id = :cat_id');
                $builder->setParameter('cat_id', $filter->catId);
            }
            $settings = App::getInstance()->getSlim()->getContainer()->get('settings');
            if($filter->geoId && array_key_exists($filter->geoId, $settings['refs']['geo']['request'])) {
                $builder->andWhere('p.geo_id = :geo_id');
                $builder->setParameter('geo_id', $settings['refs']['geo']['request'][$filter->geoId]);
            }
            $objects = $builder->getQuery()->getResult();
            $settings = App::getInstance()->getSlim()->getContainer()->get('settings');
            $staticPath = $settings['web_static_path'] . 'pics/';
            foreach ($objects as $object) {
                $key = $object['product_id'] . $object['kit_id'];
                if (array_key_exists($key, $products)) {
                    $products[$key]['items'][] = array(
                        'name' => $object['item_name'],
                        'width' => $object['item_width'],
                        'height' => $object['item_height'],
                        'count' => $object['item_count']
                    );
                } else {
                    $products[$key] = array(
                        'name' => $object['name'],
                        'img' => $object['img'],
                        'images' => array(
                            'original' => $staticPath . $object['img'],
                            'small' => $staticPath . 'm_' . $object['img'],
                            'medium' => $staticPath . 'b_' . $object['img']
                        ),
                        'composition' => $object['composition'],
                        'material' => $object['material'],
                        'country' => $object['country'],
                        'event' => $object['event'],
                        'price' => $object['kit_price'],
                        'old_price' => $object['old_kit_price'],
                        'items' => array(
                            array(
                                'name' => $object['item_name'],
                                'width' => $object['item_width'],
                                'height' => $object['item_height'],
                                'count' => $object['item_count']
                            )
                        )
                    );
                }
            }
            $products = array_values($products);
        }
        return $products;
    }

}