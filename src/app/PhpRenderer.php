<?php
namespace App;

class PhpRenderer extends \Slim\Views\PhpRenderer{
    public function renderPartial($template, array $data = []){
        $output = $this->fetch($template, $data);
        return $output;
    }
}