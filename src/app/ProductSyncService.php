<?php

namespace App;

use App\Integrations\IIntegration;
use Slim\Container;

class ProductSyncService
{
    private $container;

    /**
     * @var IIntegration[]
     */
    private $integrationsList = array();

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function checkout(IIntegration $integration = null)
    {
        if($integration){
            if(is_string($integration)){
                $integration = new $integration($this->container);
            }
            $this->integrationsList = array($integration);
        } else {
            $settings = $this->container->get('settings');
            $integrations = $settings['integrations'];
            foreach($integrations as $class => $params){
                $params['container'] = $this->container;
                $this->integrationsList[] = new $class($params);
            }
        }

        foreach($this->integrationsList as $item){
            $item->checkout();
        }
    }
}