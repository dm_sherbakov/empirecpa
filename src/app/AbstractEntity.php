<?php

namespace App;

use Doctrine\ORM\EntityManager;

abstract class AbstractEntity
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager = null;

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        if ($this->entityManager === null) {
            $this->entityManager = $this->createEntityManager();
        }

        return $this->entityManager;
    }

    /**
     * @return EntityManager
     */
    public function createEntityManager()
    {
        return App::getInstance()->getSlim()->getContainer()->get(EntityManager::class);
    }

}