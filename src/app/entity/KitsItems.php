<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class KitsItems
 * @ORM\Entity
 * @ORM\Table(name="kits_items")
 */
class KitsItems
{
    use TCopyToArray;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $kit_id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $item_id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $count;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $kit_id
     */
    public function setKitId($kit_id)
    {
        $this->kit_id = $kit_id;
    }

    /**
     * @param mixed $item_id
     */
    public function setItemId($item_id)
    {
        $this->item_id = $item_id;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @ORM\return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @OMR\return integer
     */
    public function getKitId()
    {
        return $this->kit_id;
    }

    /**
     * @ORM\return integer
     */
    public function getItemId()
    {
        return $this->item_id;
    }

    /**
     * @OMR\return integer
     */
    public function getCount()
    {
        return $this->count;
    }

}