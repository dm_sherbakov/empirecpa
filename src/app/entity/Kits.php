<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Kits
 * @ORM\Entity
 * @ORM\Table(name="kits")
 */
class Kits
{
    use TCopyToArray;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $name;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


    /**
     * @ORM\return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\return string
     */
    public function getName()
    {
        return $this->name;
    }

}