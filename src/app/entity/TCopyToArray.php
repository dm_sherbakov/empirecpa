<?php
namespace App\Entity;

trait TCopyToArray{
    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}