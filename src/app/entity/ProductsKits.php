<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ProductsKits
 * @ORM\Entity
 * @ORM\Table(name="products_kits")
 */
class ProductsKits
{
    use TCopyToArray;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $product_id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $kit_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $old_price;


    /**
     * @ORM\Column(type="integer")
     */
    protected $is_active;

    /**
     * @ORM\Column(type="integer")
     */
    protected $kit_number;

    /**
     * @return mixed
     */
    public function getKitNumber()
    {
        return $this->kit_number;
    }

    /**
     * @param mixed $kit_number
     */
    public function setKitNumber($kit_number)
    {
        $this->kit_number = $kit_number;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @param mixed $kit_id
     */
    public function setKitId($kit_id)
    {
        $this->kit_id = $kit_id;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param mixed $old_price
     */
    public function setOldPrice($old_price)
    {
        $this->old_price = $old_price;
    }

    /**
     * @param mixed $is_active
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }


    /**
     * @ORM\return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\return integer
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @ORM\return integer
     */
    public function getKitId()
    {
        return $this->kit_id;
    }

    /**
     * @ORM\return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @ORM\return integer
     */
    public function getOldPrice()
    {
        return $this->old_price;
    }

    /**
     * @ORM\return integer
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

}