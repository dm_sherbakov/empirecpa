<?php

namespace App\Entity;

use \Doctrine\ORM\Mapping as ORM;

/**
 * Class Geo
 * @ORM\Entity
 * @ORM\Table(name="geo")
 */
class Geo
{
    use TCopyToArray;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $country;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @ORM\return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\return string
     */
    public function getCountry()
    {
        return $this->country;
    }


}