<?php
namespace App\Entity;

use \Doctrine\ORM\Mapping as ORM;

/**
 * Class Events
 * @ORM\Entity
 * @ORM\Table(name="events")
 */
class Events
{
    use TCopyToArray;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @ORM\Column(type="integer")
     */
    protected $active;


    /**
     * @ORM\return integer
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @ORM\return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @ORM\return integer
     */
    public function getActive()
    {
        return $this->active;
    }


}