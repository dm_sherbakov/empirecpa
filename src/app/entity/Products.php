<?php

namespace App\Entity;

use App\App;
use \Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="products")
 */
class Products
{

    use TCopyToArray;
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $external_id;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $external_id
     */
    public function setExternalId($external_id)
    {
        $this->external_id = $external_id;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @param mixed $old_price
     */
    public function setOldPrice($old_price)
    {
        $this->old_price = $old_price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param mixed $geo_id
     */
    public function setGeoId($geo_id)
    {
        $this->geo_id = $geo_id;
    }

    /**
     * @param mixed $material_id
     */
    public function setMaterialId($material_id)
    {
        $this->material_id = $material_id;
    }

    /**
     * @param mixed $composition
     */
    public function setComposition($composition)
    {
        $this->composition = $composition;
    }

    /**
     * @param mixed $event_id
     */
    public function setEventId($event_id)
    {
        $this->event_id = $event_id;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param mixed $hide
     */
    public function setHide($hide)
    {
        $this->hide = $hide;
    }

    /**
     * @ORM\Column(type="integer")
     */
    protected $number;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $img;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $old_price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $price;

    /**
     * @ORM\Column(type="string", length=255)
    */
    protected $from_server;

    /**
     * @return mixed
     */
    public function getFromServer()
    {
        return $this->from_server;
    }

    /**
     * @param mixed $from_server
     */
    public function setFromServer($from_server)
    {
        $this->from_server = $from_server;
    }

    /**
     * @ORM\Column(type="integer")
     */
    protected $geo_id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $material_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $composition;

    /**
     * @ORM\Column(type="integer")
     */
    protected $event_id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $type;

    /**
     * @ORM\Column(type="integer")
     */
    protected $hide;


    /**
     * @ORM\Column(type="integer")
     */
    protected $cat_id;


    /**
     * @ORM\return integer
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @ORM\return integer
     */
    public function getCatId()
    {
        return $this->cat_id;
    }

    /**
     * @param mixed $cat_id
     */
    public function setCatId($cat_id)
    {
        $this->cat_id = $cat_id;
    }

    /**
     * @ORM\return integer
     */
    public function getExternalId(){
        return $this->external_id;
    }

    /**
     * @ORM\return integer
     */
    public function getNumber(){
        return $this->number;
    }

    /**
     * @ORM\return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @ORM\return string
     */
    public function getImg(){
        return $this->img;
    }

    /**
     * @ORM\return integer
     */
    public function getPrice(){
        return $this->price;
    }

    /**
     * @ORM\return integer
     */
    public function getOldPrice(){
        return $this->old_price;
    }

    /**
     * @ORM\return integer
     */
    public function getGeoId(){
        return $this->geo_id;
    }

    /**
     * @ORM\return integer
     */
    public function getMaterialId(){
        return $this->material_id;
    }

    /**
     * @ORM\return integer
     */
    public function getComposition(){
        return $this->composition;
    }

    /**
     * @ORM\return integer
     */
    public function getEventId(){
        return $this->event_id;
    }

    /**
     * @ORM\return integer
     */
    public function getHide(){
        return $this->hide;
    }

    /**
     * @ORM\return integer
     */
    public function getType(){
        return $this->type;
    }

    public function getImagesPathList(){
        $settings = App::getInstance()->getSlim()->getContainer()->get('settings');
        $staticPath = $settings['static_path'];
        return array(
            'original' => $staticPath . $this->img,
            'small' => $staticPath . 'm_' . $this->img,
            'medium' => $staticPath . 'b_' . $this->img
        );
    }
}