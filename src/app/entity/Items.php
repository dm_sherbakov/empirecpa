<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Items
 * @ORM\Entity
 * @ORM\Table(name="items")
 */
class Items
{
    const TYPE_BEDSHEET = 'BEDSHEET';
    const TYPE_PILLOWCASE = 'PILLOWCASE';
    const TYPE_QUILT = 'QUILT';

    protected static $typeLabels = array(
        self::TYPE_BEDSHEET => 'Простыня',
        self::TYPE_PILLOWCASE => 'Подушка',
        self::TYPE_QUILT => 'Пододеяльник'
    );

    public static function getTypeLabels(){
        return self::$typeLabels;
    }
    use TCopyToArray;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    protected $type;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     */
    protected $height;

    /**
     * @ORM\return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @ORM\return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @ORM\return integer
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @ORM\return integer
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @ORM\Column(type="integer")
     */
    protected $width;
}