<?php
namespace App\Integrations;
use Slim\Container;

interface IIntegration{
    function __construct($params);
    function getProducts();
    function checkout();
}