<?php

namespace App\Integrations;

use App\App;
use Psr\Log\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Slim\Container;
use App\Entity;

class EmpireCpa implements IIntegration
{
    private $hosts = array();
    private $imgPath = 'public/storage/';
    private $categoryPath = 'category/';
    private $container;

    private static $itemsTypeRels = array(
        "ПРОСТЫНЯ" => Entity\Items::TYPE_BEDSHEET,
        "ПОДОДЕЯЛЬНИК" => Entity\Items::TYPE_QUILT,
        "НАВОЛОЧКА" => Entity\Items::TYPE_PILLOWCASE
    );

    public function __construct($params)
    {
        if(array_key_exists('container', $params) && $params['container'] instanceof Container) {
            $this->container = $params['container'];
        }
        if(array_key_exists('servers', $params)){
            $this->hosts = $params['servers'];
        }
    }

    private $urlParamsList = array(
        array(
            'type' => 999,
            'category' => 1
        ),
        array(
            'type' => 999,
            'category' => 2
        ),
        array(
            'type' => 999,
            'category' => 3
        ),
        array(
            'type' => 999,
            'category' => 4
        )
    );

    public function getProducts()
    {
        $adapter = $this->container->get('httpAdapter');
        $products = [];
        foreach($this->hosts as $hostKey => $host) {
            foreach ($this->urlParamsList as $urlParams) {
                $response = $adapter->get($host . $this->categoryPath, ['query' => $urlParams]);
                if ($response->getStatusCode() === 200) {
                    $responseEncoded = \GuzzleHttp\json_decode($response->getBody(), true);
                    foreach ($responseEncoded as &$item) {
                        $item['cat_id'] = $urlParams['category'];
                        $item['from_server'] = $hostKey;
                    }
                    $products = array_merge($products, $responseEncoded);
                }
            }
        }
        return $products;
    }

    private function checkoutImg($imgName, $host){
        try {
            $sizes = array('', 'b_', 'm_');
            $adapter = $this->container->get('httpAdapter');
            $settings = App::getInstance()->getSlim()->getContainer()->get('settings');
            $staticPath = $settings['static_path'];

            foreach($sizes as $size){
                $resFile = fopen($staticPath . 'pics' . DIRECTORY_SEPARATOR . $size . $imgName, 'w+');
                $adapter->get($host . $this->imgPath . $size . $imgName, ['save_to' => $resFile]);

            }
            return true;
        } catch (\Exception $e) {
            // Log the error or something
            return false;
        }

    }

    public function checkout()
    {
        $em = $this->container->get('em');
        foreach ($this->getProducts() as $data) {
            $geoEntity = $em->getRepository('App\Entity\Geo')->findOneBy(array('country' => $data['country']));
            if(!$geoEntity){
                $geoEntity = new Entity\Geo();
                $geoEntity->setCountry($data['country']);
                $em->persist($geoEntity);
                $em->flush();
            }

            $materialEntity = $em->getRepository('App\Entity\Materials')->findOneBy(array('name' => $data['material']));
            if(!$materialEntity){
                $materialEntity = new Entity\Materials();
                $materialEntity->setName($data['material']);
                $em->persist($materialEntity);
                $em->flush();
            }

            $eventEntity = $em->getRepository('App\Entity\Events')->findOneBy(array('name' => $data['action']));
            if(!$eventEntity){
                $eventEntity = new Entity\Events();
                $eventEntity->setName($data['action']);
            }
            $eventEntity->setActive($data['action_active']);
            $em->persist($eventEntity);
            $em->flush();

            $productEntity = $em->getRepository('App\Entity\Products')->findOneBy(array('external_id' => $data['id']));
            if(!$productEntity) {
                $productEntity = new Entity\Products();
            }

            $productEntity->setExternalId($data['id']);
            $productEntity->setNumber($data['number']);
            $productEntity->setName($data['name']);
            $productEntity->setImg($data['img']);
            $productEntity->setComposition($data['composition']);
            $productEntity->setHide($data['hide']);
            $productEntity->setType($data['type']);
            $productEntity->setPrice($data['price_5']);
            $productEntity->setOldPrice($data['old_price_5']);
            $productEntity->setCatId($data['cat_id']);
            $productEntity->setGeoId($geoEntity->getId());
            $productEntity->setMaterialId($materialEntity->getId());
            $productEntity->setEventId($eventEntity->getId());
            $productEntity->setFromServer($data['from_server']);
            $em->persist($productEntity);
            $em->flush();

            if(array_key_exists($data['from_server'], $this->hosts)){
                $this->checkoutImg($data['img'], $this->hosts[$data['from_server']]);
            } else {
                throw new InvalidArgumentException('From server key not found in config list: ' . $data['from_server']);
            }

            $parsedKits = array(
                1 => $this->parseKitString($data['kit_1']),
                2 => $this->parseKitString($data['kit_2']),
                3 => $this->parseKitString($data['kit_3']),
                4 => $this->parseKitString($data['kit_4'])
            );
            $parsedKits = array_filter($parsedKits);
            foreach($parsedKits as $kitNumber => $parsedKitItems){
                $productKitEntity = $em->getRepository('App\Entity\ProductsKits')->findOneBy(array(
                    'product_id' => $productEntity->getId(),
                    'kit_number' => $kitNumber
                ));
                if($productKitEntity){
                    $kitEntity = $em->getRepository('App\Entity\Kits')->find($productKitEntity->getKitId());
                } else {
                    $kitEntity = new Entity\Kits();
                    $productKitEntity = new Entity\ProductsKits();
                    $productKitEntity->setProductId($productEntity->getId());
                    $productKitEntity->setKitNumber($kitNumber);
                }
                $kitEntity->setName($data['kit_' . $kitNumber]);
                $em->persist($kitEntity);
                $em->flush();

                $productKitEntity->setKitId($kitEntity->getId());
                $productKitEntity->setPrice($data['price_' . $kitNumber]);
                $productKitEntity->setOldPrice($data['old_price_' . $kitNumber]);
                $productKitEntity->setIsActive($data['is_active_' . $kitNumber]);
                $em->persist($productKitEntity);
                $em->flush();


                foreach($parsedKitItems as $parsedItem) {
                    $item = $em->getRepository('App\Entity\Items')->findOneBy(array(
                        'type' => $parsedItem['type'],
                        'height' => $parsedItem['height'],
                        'width' => $parsedItem['width']
                    ));
                    if (!$item) {
                        $item = new Entity\Items();
                        $item->setType($parsedItem['type']);
                        $item->setHeight($parsedItem['height']);
                        $item->setWidth($parsedItem['width']);
                        $item->setName($parsedItem['name']);
                        $em->persist($item);
                        $em->flush();
                    }
                    $kitItemsEntity = $em->getRepository('App\Entity\KitsItems')->findOneBy(array(
                        'item_id' => $item->getId(),
                        'kit_id' => $kitEntity->getId()
                    ));
                    if(!$kitItemsEntity){
                        $kitItemsEntity = new Entity\KitsItems();
                        $kitItemsEntity->setItemId($item->getId());
                        $kitItemsEntity->setKitId($kitEntity->getId());
                    }
                    $kitItemsEntity->setCount($parsedItem['count']);
                    $em->persist($kitItemsEntity);
                    $em->flush();
                }

            }
        }
    }

    private function parseKitString($source){
        $result = array();
        /*
         * ПРОСТЫНЯ: 200Х215 - 1 ШТ.
         * ПОДОДЕЯЛЬНИК: 180Х215 - 1 ШТ.
         * НАВОЛОЧКА: 70Х70 - 2 ШТ.
         */
        if(preg_match_all('/(?<type>[а-яА-Я]+):\s(?<height>\d+)Х(?<width>\d+)\s\-\s(?<count>\d+)\s(ШТ|шт)?./', $source, $matches)){
            $types = $matches['type'];
            $heights = $matches['height'];
            $widths = $matches['width'];
            $counts = $matches['count'];


            foreach($types as $id => $type){
                if(array_key_exists($type, self::$itemsTypeRels)){
                    $result[] = array(
                        'type' => self::$itemsTypeRels[$type],
                        'name' => $type,
                        'height' => $heights[$id],
                        'width' => $widths[$id],
                        'count' => $counts[$id]
                    );
                }
            }
        }
        return $result;
    }
}