<?php
require __DIR__ . '../vendor/autoload.php';
$baseAppPath = implode(DIRECTORY_SEPARATOR, array(
    'src',
    'app',
    'BaseApp.php'
));
include_once $baseAppPath;
