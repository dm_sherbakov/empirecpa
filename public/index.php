<?php
include_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'bootstrap.php';
$appPath = implode(DIRECTORY_SEPARATOR, array(
    __DIR__,
    '..',
    'src',
    'app',
    'App.php'
));

include_once $appPath;

$app = App\App::getInstance();
$app->getSlim()->run();