<html class=""><head>
    <meta charset="utf-8">
    <title>Постельное бельё</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="static/css/jquery.fancybox.min.css">
    <link rel="stylesheet" type="text/css" href="static/css/app.min.css">

    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <style type="text/css">.fancybox-margin{margin-right:17px;}</style></head><body class="">
<header class="first_color">
    <div class="wrapper row">
        <div class="contacts">
            <!-- <div class="number">+8 (800) <strong>333-48-75</strong></div> -->
            <span>Заявки на сайте 24 часа</span>
        </div>
        <div class="logo" style="position: relative;">

            <a href="#">Постельное бельё
                <span>Магазин №1 в России</span>
            </a>
        </div>
        <ul class="headline">
            <li>
                <span>15</span>
                Более 15 лет успешной работы
            </li>
            <li>
                <span>250 000</span>
                Уже более 250 000 довольных клиентов
            </li>
            <li>
                <span>3</span>
                Каждый 3-ий клиент становится постоянным!
            </li>
        </ul>

    </div>
</header>
<div class="header_info first_color">
    <div class="wrapper">
        <h1> <span class="subhead">					ПОСТЕЛЬНОЕ БЕЛЬЕ ОТ </span><br> <span class="subhead">					ПРОИЗВОДИТЕЛЯ №1 В РОССИИ</span><br> 				</h1>
        <div class="text">Свое производство. 100% хлопок. 15 лет успешной работы.</div>
    </div>
</div>

<div class="sale_box second_color">
    <div class="wrapper row">

        <div class="text">Только 1 ДЕНЬ<span>Скидки до 60%</span>на всю продукцию!</div>
        <!-- <div class="offer desctop">Каждому покупателю полотенце В ПОДАРОК</div> -->
        <div class="offer desctop" style="padding-top: 10px">При заказе от 3900 руб. ДОСТАВКА БЕСПЛАТНО!</div>

        <div class="timer_box" style="margin: 20px auto 0; float: none;">
            <div class="caption">До конца распродажи:</div>
            <ul>
                <li class="TimerDays">0</li>
                <li class="TimerHours">08</li>
                <li class="TimerMinutes">19</li>
                <li class="TimerSeconds">52</li>
            </ul>
        </div>
        <!-- 	<div class="offer mobile">Каждому покупателю полотенце В ПОДАРОК</div>
            <div class="offer mobile" style="padding-top: 10px">При заказе от 3900 руб. ДОСТАВКА БЕСПЛАТНО!</div>
            <div class="image"><img src="img/milo.png" alt=""></div> -->
    </div>
</div>

<div class="catalog_box second_color">
    <div class="wrapper">
        <h2>Каталог</h2>
        <div class="categories_current">
            Вы в категории: <span id="current-size">1,5-спальные</span>
        </div>
        <div class="categories_btn_box onecat">
            <a data-category="1" class="active">1,5-спальные</a>
            <a class="" data-category="2">2-спальные</a>
            <a data-category="3" class="">евро</a>
            <a data-category="4" class="">семейные</a>

        </div>
        <div class="type">
            <h3 style="font-size: 34px; color: #ee469d" id="category">Категории:</h3>

            <label for="type_1" style="margin-top: 6px; color: rgb(0, 0, 0);">
                <span style="float: left;">Цветы </span>
                <input type="checkbox" name="type_1" data=".type_1" id="type_1" class="chooseType"><label for="type_1" class="label-img"></label>
            </label>

            <label for="type_2" style="margin-top: 6px; color: rgb(52, 152, 219);">
                <span style="float: left;">Животные </span>
                <input type="checkbox" name="type_2" data=".type_2" id="type_2" class="chooseType"><label for="type_2" class="label-img"></label>
            </label>

            <label for="type_3" style="margin-top: 6px; color: rgb(0, 0, 0);">
                <span style="float: left;">Однотонные </span>
                <input type="checkbox" name="type_3" data=".type_3" id="type_3" class="chooseType"><label for="type_3" class="label-img"></label>
            </label>


            <label for="type_4" style="margin-top: 6px; color: rgb(0, 0, 0);">
                <span style="float: left;">Узоры </span>
                <input type="checkbox" name="type_4" data=".type_4" id="type_4" class="chooseType"><label for="type_4" class="label-img"></label>
            </label>

        </div>
        <!--
        <div class="categories_form_box">
            Категории:
            <form>
                <input type="checkbox" id="flowers" checked>
                <label for="flowers">цветы</label>
                <input type="checkbox" id="animals">
                <label for="animals">животные</label>
                <input type="checkbox" id="mono">
                <label for="mono">однотонные</label>
                <input type="checkbox" id="pattern">
                <label for="pattern">узоры</label>
            </form>
        </div>
        -->
        <?=$this->renderPartial('product_list.php', array('products' => $products))?>
        <div class="categories_current">
            Вы в категории: <span>1,5-спальные</span>
        </div>
        <div class="categories_btn_box">
            <a data-category="1" class="active">1,5-спальные</a>
            <a class="" data-category="2">2-спальные</a>
            <a data-category="3" class="">евро</a>
            <a data-category="4" class="">семейные</a>

        </div>
    </div>
</div>

<div class="sale_box first_color">
    <div class="wrapper row">

        <div class="text">Только 1 ДЕНЬ<span>Скидки до 60%</span>на всю продукцию!</div>

        <div class="timer_box" style="margin-top: 35px">
            <div class="caption">До конца распродажи:</div>
            <ul>
                <li class="TimerDays">0</li>
                <li class="TimerHours">08</li>
                <li class="TimerMinutes">19</li>
                <li class="TimerSeconds">52</li>
            </ul>
        </div>
    </div>
</div>
<div class="privilege_box second_color">
    <div class="wrapper">
        <ul>
            <li>
                <div class="icon_border">
                    <div class="icon price"></div>
                </div>
                <div class="text">
							<span>
								ЦЕНЫ ПРОИЗВОДИТЕЛЯ БЕЗ ПОСРЕДНИКОВ
							</span>
                    все комплекты постельного белья, представленные на сайте, присутствуют в наличии у нас на складе. Вы можете получить свой комплект уже на следующий день.
                </div>
            </li>
            <li>
                <div class="icon_border">
                    <div class="icon quality"></div>
                </div>
                <div class="text">
							<span>
								ЕВРОПЕЙСКОЕ КАЧЕСТВО
							</span>
                    постельное белье состоит из 100% хлопка, не рвется, не усаживается, не линяет. проверено веременем, нашими клиентами и нашей службой контроля качества.
                </div>
            </li>
            <li>
                <div class="icon_border">
                    <div class="icon guarantee"></div>
                </div>
                <div class="text">
							<span>
								ГАРАНТИЯ ВОЗВРАТА ДЕНЕГ
							</span>
                    если вам не подойдет комплект, вы захотите обменять его на другой, или попросту сдать - мы вернем вам 100% денег. вы ничем не рискуете.
                </div>
            </li>
            <li>
                <div class="icon_border">
                    <div class="icon pay"></div>
                </div>
                <div class="text">
							<span>
								ОПЛАТА ПРИ ПОЛУЧЕНИИ КУРЬЕРУ
							</span>
                    мы сотрудничаем с курьерской службой доставки по москве, поэтому ваш заказ будет доставлен вам прямо домой.
                </div>
            </li>
            <li>
                <div class="icon_border">
                    <div class="icon production"></div>
                </div>
                <div class="text">
							<span>
								СВОЕ ПРОИЗВОДСТВО
							</span>
                    поэтому мы можем предложить вам самые выгодные цены, а также самое высокое качество постельного белья в россии
                </div>
            </li>
            <li>
                <div class="icon_border">
                    <div class="icon delivery"></div>
                </div>
                <div class="text">
							<span>
								БЫСТРАЯ ДОСТАВКА
							</span>
                    доставляем по всей России
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="table_box first_color">
    <div class="wrapper">
        <h2>Таблица Размеров</h2>
        <table class="desctop_table">
            <thead>
            <tr>
                <th>Размер</th>
                <th>1.5 - спальный</th>
                <th>2.0 - спальный</th>
                <th>Евро</th>
                <th>Семейный</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>Простыня</th>
                <td>150х215 - 1 шт</td>
                <td>180х215 - 1 шт</td>
                <td>215х240 - 1 шт </td>
                <td>215х240 - 1 шт </td>
            </tr>
            <tr>
                <th>Пододеяльник</th>
                <td>150x215 - 1 шт</td>
                <td>180х215 - 1 шт</td>
                <td>200х215 - 1 шт</td>
                <td>150х215 - 2 шт</td>
            </tr>
            <tr>
                <th>Наволочка</th>
                <td>70x70 - 2 шт</td>
                <td>70х70 - 2 шт</td>
                <td>70х70 - 2 шт  |  50х70 - 2 шт</td>
                <td>70х70 - 2 шт  |  50х70 - 2 шт</td>
            </tr>
            </tbody>
        </table>
        <table class="mobile_table">
            <thead>
            <tr>
                <th>1,5 - спальный</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Простыня 150х215 - 1 шт</td>
            </tr>
            <tr>
                <td>пододеяльник 150x215 - 1 шт</td>
            </tr>
            <tr>
                <td>наволочка 70x70 - 2 шт</td>
            </tr>
            </tbody>
        </table>
        <table class="mobile_table">
            <thead>
            <tr>
                <th>2.0 - спальный</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Простыня 180х215 - 1 шт</td>
            </tr>
            <tr>
                <td>Пододеяльник 180х215 - 1 шт</td>
            </tr>
            <tr>
                <td>Наволочка 70х70 - 2 шт</td>
            </tr>
            </tbody>
        </table>
        <table class="mobile_table">
            <thead>
            <tr>
                <th>Евро</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Простыня 215х240 - 1 шт</td>
            </tr>
            <tr>
                <td>Пододеяльник 200х215 - 1 шт</td>
            </tr>
            <tr>
                <td>Наволочка 70х70 - 2 шт</td>
            </tr>
            <tr>
                <td>Наволочка 50х70 - 2 шт</td>
            </tr>
            </tbody>
        </table>
        <table class="mobile_table">
            <thead>
            <tr>
                <th>Семейный</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Простыня 215х240 - 1 шт</td>
            </tr>
            <tr>
                <td>Пододеяльник 150х215 - 2 шт</td>
            </tr>
            <tr>
                <td>Наволочка 70х70 - 2 шт</td>
            </tr>
            <tr>
                <td>Наволочка 50х70 - 2 шт</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="chart_box first_color">
    <div class="wrapper">
        <h2>Схема работы</h2>
        <ul class="row">
            <li>
                <span>1</span>
                <div class="text">
                    Сделайте заказ<br> на сайте</div>
            </li>
            <li>
                <span>2</span>
                <div class="text">Наши менеджеры свяжутся<br>
                    с вами для уточнения деталей</div>
            </li>
            <li>
                <span>3</span>
                <div class="text">Мы отправим Ваш заказ<br>
                    Почтой России или курьером</div>
            </li>
            <li>
                <span>4</span>
                <div class="text">Вы наслаждаетесь качеством<br>
                    и мягкостью нашего постельного<br>
                    белья!</div>
            </li>
        </ul>
    </div>
</div>
<div class="certificates_box second_color">
    <div class="wrapper">
        <h2>Сертификаты</h2>
        <ul style="max-width: 600px; margin: 0 auto;">
            <li><img src="img/sert11.jpg" alt=""></li>
            <li><img src="img/sert22.jpg" alt=""></li>
            <li><img src="img/sert33.jpg" alt=""></li>
        </ul>
    </div>
</div>
<div class="reviews_box first_color">
    <div class="wrapper">
        <h2>Отзывы</h2>
        <div class="nametabs">
            <a href="#tab-text" class="selected">Текстовые отзывы</a>
            <a href="#tab-video">Видеоотзывы</a>
        </div>
        <div class="tab-content" id="tab-text" style="display: block;">
            <div class="row text-center">
                <div class="col-sm-12 col-xs-12">

                    <div class="review">
                        <h4>Анна</h4>
                        <img src="comments/otzyv_1.png" alt="" class="face">
                        <div class="text">
                            <p>Боялась покупать через интернет. Постельное белье такой товар, который хочется пощупать, посмотреть. Не пахнет чем химическим узнать. Но нашла очень красивую расцветку и решила все же заказать! Заказ пришел через два дня. Дизайн полностью соответствует изображению, материалы очень качественные. Буду теперь покупать только у Вас! Спасибо за скидки!</p>
                        </div>
                        <img src="comments/1.jpg" alt="" class="photo">
                    </div>

                </div>

                <div class="col-sm-12 col-xs-12">

                    <div class="review">
                        <h4>Надежда</h4>
                        <img src="comments/otzyv_2.png" alt="" class="face">
                        <div class="text">
                            <p>Честно говоря, не ожидала такого качества! Порадовали! Качество пошива и самих материалов очень высокие. Сама когда-то работала на швейной фабрике и понимаю, о чем говорю :) Рада что ещё остались компании, которые не экономят на качестве и заботятся о своих клиентах! Швы ровные и красивые, нитки не расходятся после стирки, цвет не теряется!</p>

                        </div>
                        <img src="comments/2.jpg" alt="" class="photo">
                    </div>

                </div>
            </div>

            <div class="row text-center">
                <div class="col-sm-12 col-xs-12">

                    <div class="review">
                        <h4>Екатерина</h4>
                        <img src="comments/otzyv_3.png" alt="" class="face">
                        <div class="text">
                            <p>Спать на таком белье одно удовольствие! Раньше не особо обращала внимание на материал, а теперь понимаю, сколько часов отличного сна потеряла. Очень жаль, что не нашла вас раньше! Зато теперь буду брать только тут. Соотношение цена-качество для меня идеальные. Спасибо за быструю доставку!</p>
                        </div>
                        <img src="comments/3.jpg" alt="" class="photo">
                    </div>

                </div>

                <div class="col-sm-12 col-xs-12">

                    <div class="review">
                        <h4>Александра</h4>
                        <img src="comments/otzyv_4.png" alt="" class="face">
                        <div class="text">
                            <p>У меня просто глаза разбежались, такой огромный выбор, и все хочется забрать :-)) Не смогла сама определиться с размерами, поэтому проконсультировалась с девушкой-менеджером (перезвонили после того как оставила заявку). Она мне все очень доходчиво объяснила и помогла определиться со всем! Спасибо таком грамотным специалистам, очень порадовали! Белье уже пришло, отправляю фотографию, все как на картинке!</p>
                        </div>
                        <img src="comments/4.jpg" alt="" class="photo">
                    </div>

                </div>
            </div>

            <div class="show-more">
                <div class="row text-center">
                    <div class="col-sm-12 col-xs-12">

                        <div class="review">
                            <h4>Наталья</h4>
                            <img src="comments/otzyv_5.png" alt="" class="face">
                            <div class="text">
                                <p>На вашем белье очень приятно спать. Отдельное спасибо за скидку, я такое белье не могла найти дешевле 3000 рублей, а с вами сильно сэкономила без ущерба качеству! Теперь я точно ваш постоянный клиент! Вы молодцы!</p>
                            </div>
                            <img src="comments/5.jpg" alt="" class="photo">
                        </div>

                    </div>

                    <div class="col-sm-12 col-xs-12">

                        <div class="review">
                            <h4>Марина</h4>
                            <img src="comments/otzyv_6.png" alt="" class="face">
                            <div class="text">
                                <p>Покупала в подарок сестре на свадьбу! Она очень довольна! Говорит что подарили кучу комплектов, но больше всего понравился мой подарок! Рада что купил именно у вас.</p>
                            </div>
                            <img src="comments/6.jpg" alt="" class="photo">
                        </div>

                    </div>
                </div>

                <div class="row text-center">
                    <div class="col-sm-12 col-xs-12">

                        <div class="review">
                            <h4>Ирина</h4>
                            <img src="comments/otzyv_7.png" alt="" class="face">
                            <div class="text">
                                <p>Я очень рада, что нашла ваш магазин! Всё как на фото, бельё не садится и не красится, спать приятно и удобно. Приятно покупать у производителя ещё и за такие адекватные деньги!</p>
                            </div>
                            <img src="comments/7.jpg" alt="" class="photo">
                        </div>

                    </div>

                    <div class="col-sm-12 col-xs-12">

                        <div class="review">
                            <h4>Ольга</h4>
                            <img src="comments/otzyv_8.png" alt="" class="face">
                            <div class="text">
                                <p>Уже давно приспособилась покупать товары через интернет. Это дешевле и намного удобнее, но вот белье еще не заказывала ни разу. Решила попробовать, когда увидела у вас очаровательные комплекты! Осталась довольна качеством, белье прослужит долгие годы, я в этом уверена. Прошито очень хорошо, белье приятное на ощупь, на нем действительно хочется спать :) Спасибо! Процветания вам!</p>
                            </div>
                            <img src="comments/8.jpg" alt="" class="photo">
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content" id="tab-video" style="display: none;">
            <ul>
                <li><video style="background: black;" width="415" height="293" controls="" poster="img/preview_1.png"><source src="video/review1.mp4" type="video/mp4"></video></li>
                <li><video style="background: black;" width="415" height="293" controls="" poster="img/preview_2.png"><source src="video/review2.mp4" type="video/mp4"></video></li>
                <li><video style="background: black;" width="415" height="293" controls="" poster="img/preview_3.png"><source src="video/review3.mp4" type="video/mp4"></video></li>
                <li><video style="background: black;" width="415" height="293" controls="" poster="img/preview_4.png"><source src="video/review4.mp4" type="video/mp4"></video></li>
            </ul>
        </div>
    </div>
</div>
<div class="sale_box second_color">
    <div class="wrapper row">

        <div class="text">Только 1 ДЕНЬ<span>Скидки до 60%</span>на всю продукцию!</div>

        <div class="timer_box" style="margin-top: 35px">
            <div class="caption">До конца распродажи:</div>
            <ul>
                <li class="TimerDays">0</li>
                <li class="TimerHours">08</li>
                <li class="TimerMinutes">19</li>
                <li class="TimerSeconds">52</li>
            </ul>
        </div>


    </div>
</div>
<footer class="first_color">
    <div class="wrapper">
        <div class="logo" style="position: relative;">
            <a href="#">Постельное бельё
                <span>Магазин №1 в России</span>
            </a>
        </div>
        <div class="info">
            ООО "МатексЛайн" ИНН 7723422857<br>
            ОГРН 5157746157025 г.Москва, ул. 1-я Курьяновская, д.20/1 стр.1, комн. 8<br><br>
            <a href="politics.html" target="_blank">Политика конфиденциальности</a><br>
            <span style="font-size: 8px;">*расцветка и состав могут отличаться от заявленных</span> <br>
            <span style="font-size: 10px;">**все текстильные изделия перед применением рекомендуем постирать</span>
        </div>
        <div class="contacts">
            <!-- <div class="number">+8 (800) <strong>333-48-75</strong></div> -->
            <span>Заявки на сайте 24 часа</span>
        </div>
    </div>
</footer>

<div id="scrollup" style="display: block;">
    <img alt="Прокрутить вверх" src="img/up-64.png" style="width: 35px; padding-top: 5px; opacity: 1;">
</div>

<div class="shadow_site"></div>
<div class="modal_contain">
    <div class="modal_window order_modal">
        <div class="close"></div>
        <h2>ЗАКАЗАТЬ СО СКИДКОЙ 60%</h2>
        <div class="image"><img src="img/pics/module_bed_img.jpg" alt=""></div>
        <div class="text">
            <p class="name_span_h2"></p>
            <p class="p-price">Цена сейчас: <span class="price_span">1990</span> <span class="price_cur">руб</span> + <b>подарок</b></p>
            <p class="p-old-price">Внимание! Цена завтра: <span class="price_span_old">7790</span> руб</p>
            <p class="p-size">Размер: <span class="size_span">2 спальные</span></p>
        </div>
        <form method="post" action="mailer.php" onsubmit="if(this.name.value==''){alert('Введите Ваше имя!');return false}if(this.phone.value==''){alert('Введите Ваш номер телефона!');return false}return true;">
            <input type="hidden" name="price">
            <input type="hidden" name="articul">
            <input type="hidden" name="cat">
            <input type="text" name="name" placeholder="ВВЕДИТЕ ИМЯ" required="">
            <input type="text" name="user_phone" placeholder="ВВЕДИТЕ ТЕЛЕФОН" required="">
            <input type="hidden" value="5af94a7171add832eb1b28e0" name="clickID">
            <input type="hidden" name="clientTime" id="user_time">
            <input type="submit" value="ЗАКАЗАТЬ ПО АКЦИИ">
        </form>
    </div>
</div>

<div class="modal_contain_clbk">
    <div class="modal_window callback_modal">
        <div class="close"></div>
        <h2>Заказать звонок</h2>
        <form method="post" action="mailer.php" onsubmit="if(this.name.value==''){alert('Введите Ваше имя!');return false}if(this.phone.value==''){alert('Введите Ваш номер телефона!');return false}return true;">
            <input type="text" name="name" placeholder="ВВЕДИТЕ ИМЯ" required="">
            <input type="text" name="user_phone" placeholder="ВВЕДИТЕ ТЕЛЕФОН" required="">
            <input type="hidden" value="5af94a7171add832eb1b28e0" name="clickID">
            <input type="hidden" name="clientTime" id="user_time">
            <input type="submit" value="ЗАКАЗАТЬ ЗВОНОК">
        </form>
    </div>
</div>





<div class="hidden" style="display: none;">
    <noindex>
        <div class="popup-holder">
            <div class="popup lightbox">

                <form id="form_1" method="post" action="mailer.php" onsubmit="if(this.name.value==''){alert('Введите Ваше имя!');return false}if(this.phone.value==''){alert('Введите Ваш номер телефона!');return false}return true;" class="order-form">
                    <fieldset>
                        <div align="center"><strong class="title" style="font-size:20px; margin-bottom:10px; border-bottom:1px solid red; color:red">Закажите сейчас! Акция заканчивается:</strong>
                            <img id="_img" src="http://mng.mir-postel.ru/public/storage/b_D005.jpg" alt="" align="left" style="width: 160px;margin: 15px 0 0 0; box-shadow: 0 3px 3px 0 rgba(0,0,0,0.5);">
                            <strong class="title" style="font-size:20px; margin-bottom:5px;" id="_title"></strong>
                            <strong class="title" style="font-size:20px; margin-bottom:5px;" id="_cat"></strong>
                            <div style="clear:both:"></div>
                            <div style="border:1px solid green; padding:3px; background:#f0fff5;">
                                <strong class="title" style="margin-bottom:0px;">Цена сейчас: <span style="color:green" id="_price">2670 </span> <span style="color:green; font-size:16px;">₽</span></strong>
                                <strong class="title" style="font-size:16px; color:#0249da; margin-bottom:2px; ">+ ПОЛОТЕНЦЕ В ПОДАРОК! </strong></div>
                            <strong class="title" style="font-size:16px; text-align: right; padding-right: 40px;color:#616161; margin-bottom:0px;">Цена через <b>3 часа</b>: <span style="color:red" id="_old">9600 </span> <span style="color:red;font-size:16px;">₽</span> </strong>
                        </div>

                        <div style="clear:both;"></div>
                        <div style="margin-left:20px; margin-top: 10px">
                            <label for="id-1">Ваше имя:</label>
                            <div class="text" style="margin-bottom: 0px; mpadding-bottom: 0px">
                                <input type="text" name="name" id="id-1" required="">
                            </div>
                            <div style="color: #999999; font-size: 14px; margin-bottom: 10px">чтобы мы знали, как к Вам обратиться</div>
                        </div>

                        <div class="phone_req_box" style="margin-left:20px;">
                            <label for="id-2">Номер телефона:</label>
                            <div class="text" style="margin-bottom: 0px; padding-bottom: 0px">
                                <input type="text" name="user_phone" id="id-2" required="">
                            </div>
                            <div style="color: #999999; font-size: 14px; margin-bottom: 10px">чтобы мы смогли с Вами связаться и уточнить детали заказа</div>
                        </div>

                        <input type="hidden" name="articul" id="input-article" value="">
                        <input type="hidden" name="price" id="input-price" value="">
                        <input type="hidden" name="cat" id="input-category" value="">

                        <input type="hidden" value="5af94a7171add832eb1b28e0" name="clickID">
                        <input type="hidden" name="clientTime" id="user_time">
                        <div align="center"> <input type="submit" name="order802" value="Заказать →" class="zakaz_send"></div>
                    </fieldset>
                </form>
            </div>
        </div>
    </noindex>
</div>

<script src="/jquery.min.js"></script>

<script src="js/jquery.ba-outside-events.js"></script>
<script src="/jquery.maskedinput.js"></script>
<script src="js/jquery.bxslider.js"></script>
<script src="/jquery.fancybox.pack.js"></script>
<script src="js/countdown.min.js"></script>
<script src="js/lazyload.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js?v3"></script>

<script src="js/jquery.maskedinput.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        $("[name='user_phone']").mask("+7(999) 999-9999");
    });
</script>





</body></html>